//super class for a buiding
class Building{
  constructor(x,y){
    this.type = 1;
    this.x = x;
    this.y = y;
  }

  //do nothing
  step(game){
  }
}

class TownCenter extends Building{
  constructor(x, y){
    super(x,y);
    this.name = "House";
    this.image = house;
    //how many tiles tall and wide the thing is
    this.width = 3;
    this.length = 3;
    //image dimensions
    this.imHeight = 160;
    this.imWidth = 192;
    //health
    this.maxHealth = 100;
    this.health = this.maxHealth;
  }
}


class Tree extends Building{
  constructor(x, y){
    super(x,y);
    this.name = "Tree";
    this.image = tree;
    //how many tiles tall and wide the thing is
    this.width = 1;
    this.length = 1;
    //image dimensions
    this.imHeight = 160;
    this.imWidth = 64;
    //health
    this.maxHealth = 100;
    this.health = this.maxHealth;
  }
}


//IDEA this building will produce a free goblin every 5 minutes or so
class GoblinPit extends Building{
  constructor(x, y){
    super(x,y);
    this.name = "Goblin Pit";
    this.image = goblinPit;
    //how many tiles tall and wide the thing is
    this.width = 3;
    this.length = 3;
    //image dimensions
    this.imHeight = 128;
    this.imWidth = 192;
    //health
    this.maxHealth = 100;
    this.health = this.maxHealth;
    //for producing a goblin ever so often
    this.age = 0;
  }

  step(game){
    //1 free (extra weak) gobbo every 3 minutes
    if(this.age%5400 == 0){
      game.monsters.push(new Goblet(this.x - 1, this.y -1));
    }
    //TODO make a data structure for animation
    switch(this.age%100){
      case 0:
      case 15:
      case 40:
      case 60:
      case 85:
        this.image = goblinPit;
        break;
      case 10:
        this.image = goblinPit2;
        break;
      case 35:
        this.image = goblinPit3;
        break;
      case 55:
        this.image = goblinPit4;
        break;
      case 80:
        this.image = goblinPit5;
    }
    this.age++;
  }
}

class GoblinWall extends Building{
  constructor(x, y){
    super(x,y);
    this.name = "Goblin Wall";
    this.image = goblinWall;
    //how many tiles tall and wide the thing is
    this.width = 1;
    this.length = 1;
    //image dimensions
    this.imHeight = 64;
    this.imWidth = 64;
    //health
    this.maxHealth = 100;
    this.health = this.maxHealth;
  }
}

class GoblinWorkshop extends Building{
  constructor(x, y){
    super(x,y);
    this.name = "Goblin Workshop";
    this.image = goblinWorkshop;
    //how many tiles tall and wide the thing is
    this.width = 3;
    this.length = 3;
    //image dimensions
    this.imHeight = 144;
    this.imWidth = 192;
    //health
    this.maxHealth = 100;
    this.health = this.maxHealth;
  }
}
