//info about all the tiles so new ones can be added
var tileData =
[
  {
    "id": "0",
    "name": "tile",
    "passable": "no",
    //colour on the miniMap
    "r": "255",
    "g": "255",
    "b": "255"
  },
  {
    "id": "1",
    "name": "grass",
    "passable": "yes",
    "r": "0",
    "g": "160",
    "b": "0"
  },
  {
    "id": "2",
    "name": "water",
    "passable": "water",
    "r": "100",
    "g": "100",
    "b": "255"
  },
  {
    "id": "3",
    "name": "deepWater",
    "passable": "water",
    "r": "0",
    "g": "0",
    "b": "255"
  },
  {
    "id": "4",
    "name": "lavaRock",
    "passable": "yes",
    "r": "120",
    "g": "110",
    "b": "110"
  },
  {
    "id": "5",
    "name": "crystals",
    "passable": "yes",
    "r": "150",
    "g": "110",
    "b": "150"
  },
  {
    "id": "6",
    "name": "lava",
    "passable": "no",
    "r": "255",
    "g": "90",
    "b": "-"
  },
  {
    "id": "7",
    "name": "snow",
    "passable": "yes",
    "r": "255",
    "g": "255",
    "b": "255"
  },
  {
    "id": "8",
    "name": "snowyGrass",
    "passable": "yes",
    "r": "0",
    "g": "160",
    "b": "0"
  },
  {
    "id": "9",
    "name": "yellowSnow",
    "passable": "yes",
    "r": "255",
    "g": "255",
    "b": "0"
  },
  {
    "id": "10",
    "name": "rock",
    "passable": "yes",
    "r": "200",
    "g": "200",
    "b": "200"
  },
  {
    "id": "11",
    "name": "snowyRock",
    "passable": "yes",
    "r": "200",
    "g": "200",
    "b": "200"
  },
  {
    "id": "12",
    "name": "scorchedGrass",
    "passable": "yes",
    "r": "200",
    "g": "200",
    "b": "0"
  },
  {
    "id": "13",
    "name": "leaves",
    "passable": "yes",
    "r": "30",
    "g": "130",
    "b": "0"
  },
  {
    "id": "14",
    "name": "road",
    "passable": "yes",
    "r": "255",
    "g": "220",
    "b": "220"
  },
  {
    "id": "15",
    "name": "elfGrass",
    "passable": "yes",
    "r": "0",
    "g": "150",
    "b": "255"
  },
  {
    "id": "16",
    "name": "elfLitter",
    "passable": "yes",
    "r": "0",
    "g": "120",
    "b": "200"
  },
  {
    "id": "17",
    "name": "bakedEarth",
    "passable": "yes",
    "r": "120",
    "g": "70",
    "b": "0"
  },
  {
    "id": "18",
    "name": "sand",
    "passable": "yes",
    "r": "200",
    "g": "200",
    "b": "0"
  },
  {
    "id": "19",
    "name": "flowerGrass",
    "passable": "yes",
    "r": "0",
    "g": "160",
    "b": "0"
  }
];
