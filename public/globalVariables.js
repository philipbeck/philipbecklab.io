var running = false;
//map variables these should be choseable
var mapWidth = 100;
var mapHeight = mapWidth;
//zoom stuff
var zoom = 2;
var defualtHeight = 500;
var defualtWidth = 700;
var screenHeight = 500;
var screenWidth = 700;
//
var guiHeight = 150;
var guiWidth = 700;
//shift values for x and y in pixels
var shiftX = 0;
var shiftY = TILE_HEIGHT*mapHeight/2 - screenHeight/2;
//music volume
var volume = 0.5;
//game variables
var game;
//buildings
var house = document.getElementById("house");

//tiles
var tiles = [];
for(let i = 0; i < tileData.length; i++){
  tiles.push(document.getElementById(tileData[i].name));
}
//mousedown
var mouseDown = false;
var mouseCoords = {"x": 0, "y":0};
//the tile the mouse is hovering over
var mouseTile = false;
var selectedTile = 1;
//arrow keys for moving
var keys = {left: false, right: false, up: false, down:false,
            shift: false}

//this stuff might be removed/moved
var placing = false;

//map generation varibles
var terrainFocus = 0.05;
var biomeFocus = 0.01;
