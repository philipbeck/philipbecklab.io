class Game{
  constructor(){
    this.map = new Map();
    //array of where buildings are for pathfinding etc, stores references to the
    //building in that place so that the game can easily remove it if needed
    this.buildingMask = [];
    for(let i = 0; i < mapWidth; i++){
      this.buildingMask[i] = []
      for(let j = 0; j < mapHeight; j++){
        this.buildingMask[i][j] = 0;
      }
    }

    this.players = [new Player()];

    this.buildings = [];
    //these hardcoded buildings will go at some point
    this.addBuilding(new TownCenter(0,0));
    this.addBuilding(new TownCenter(6,7));

    this.addBuilding(new GoblinPit(20,20));
    this.addBuilding(new GoblinWorkshop(24,21));
    this.addBuilding(new GoblinWall(18, 17));
    this.addBuilding(new GoblinWall(18, 18));
    this.addBuilding(new GoblinWall(18, 19));
    this.addBuilding(new GoblinWall(18, 20));
    this.addBuilding(new GoblinWall(18, 21));
    this.addBuilding(new GoblinWall(18, 17));
    this.addBuilding(new GoblinWall(19, 17));
    this.addBuilding(new GoblinWall(20, 17));
    this.addBuilding(new GoblinWall(21, 17));
    this.addBuilding(new GoblinWall(22, 17));

    this.monsters = [new Goblin(4,0), new Goblin(6,6)];
    //random for now
    noise.seed(Math.random())

    this.map.generateMap(this);

    //counter for timing things in the game
    this.counter = 0;
  }

  getEntities(){
    //TODO make this only return what is visible
    let buildings = [];
    let units = [];

    //code to work out whether something should be drawn or not
    //buildings first
    for(let i = 0; i < this.buildings.length; i++){
      let b = this.buildings[i];
      let coords = getXY(b.x, b.y);
      //get the x and y position for the image to be drawn
      let x = coords.x - shiftX;
      let y = coords.y - (b.imHeight-(1+((b.length-1)/2))*TILE_HEIGHT) - shiftY;
      //check to make sure only the building that are on screen get drawn
      if(x <= screenWidth && x + b.imWidth >= 0 && y <= screenHeight && y + b.imHeight >= 0){
          buildings.push(b);
        }
    }
    //get the monsters which are on screen
    for(let i = 0; i < this.monsters.length; i++){
      let m = this.monsters[i];
      let coords = getXY(m.x, m.y);
      //x and y positions of the monsters
      let x = coords.x + m.imWidth/2 - shiftX;
      let y = coords.y - m.imHeight + TILE_HEIGHT/2 - shiftY;
      if(x <= screenWidth && x + m.imWidth >= 0 && y <= screenHeight && y + m.imWidth >= 0){
        units.push(m);
      }
    }

    let objects = buildings.concat(units);
    //sort at the end
    objects.sort(function(a,b){
      return getXY(a.x,a.y).y - getXY(b.x,b.y).y;
    });

    return {"objects": objects, "units": units};
  }

  //add a building so that the building mask is also updated
  addBuilding(building){
    let x = building.x;
    let y = building.y;
    if(x + building.width <= mapWidth && y + building.length <= mapHeight
    && x >= 0 && y >= 0){
      //check that there isn't a building there already then add the buliding to
      //the array and map
      for(let i = 0; i < building.width; i++){
        for(let j = 0; j < building.length; j++){
          if(this.buildingMask[x+i][y+j] != 0){
            return false;
          }
        }
      }
      //if there wasn't anything there add the building
      this.buildings.push(building);
      for(let i = 0; i < building.width; i++){
        for(let j = 0; j < building.length; j++){
          this.buildingMask[x+i][y+j] = building;
        }
      }
      return true;
    }
  }

  removeBuilding(building){
    let x = building.x;
    let y = building.y;
    //take the building off the mask
    for(let i = 0; i < building.width; i++){
      for(let j = 0; j < building.height; j++){
      this.buildingMask[x+i][y+j] = 0;
      }
    }
    //really long line of code to remove the building from it's array
    this.buildings.splice(this.buildings.indexOf(building),1);
  }

  //one in game turn
  step(){
    //first of all increment the counter
    this.counter++;
    //step all of the monsters
    for(let i = 0; i < this.monsters.length; i++){
      this.monsters[i].step();
    }

    for(let i = 0; i < this.buildings.length; i++){
      this.buildings[i].step(this);
    }
    //for testing
    if(this.counter%20 == 0){
      this.players[0].crystals++;
    }
  }

  //set the path for a unit
  path(unit,x,y){
    unit.getPath(x,y,this.map.map,this.buildingMask);
  }
}
